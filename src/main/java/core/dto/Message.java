package core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Message {

    private final String message;

    public static Message message(String message) {
        return new Message(message);
    }

}
