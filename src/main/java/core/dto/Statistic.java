package core.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Statistic {

    private LocalDate date;
    private double proteins;
    private double fats;
    private double carbs;
    private double kilocalories;
    private Ratio ratio;

}
