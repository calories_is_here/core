package core.dto;

import lombok.Data;

@Data
public class Ratio {

    private double proteins;
    private double fats;
    private double carbs;

    public Ratio(double proteins, double fats, double carbs) {
        double sum = proteins + fats + carbs;

        if (sum == 0) {
            this.proteins = 0;
            this.fats = 0;
            this.carbs = 0;
        } else {
            this.proteins = proteins * 100 / sum;
            this.fats = fats * 100 / sum;
            this.carbs = carbs * 100 / sum;
        }
    }
}
