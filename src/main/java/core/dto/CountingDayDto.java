package core.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
public class CountingDayDto {

    private Long id;

    private LocalDate countingDate;

    private List<DishDto> dishes;

}
