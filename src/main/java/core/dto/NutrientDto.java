package core.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Data
@NoArgsConstructor
public class NutrientDto {

    private Long id;

    @NotBlank
    @NotNull
    private String name;

    @PositiveOrZero
    private double proteins;

    @PositiveOrZero
    private double fats;

    @PositiveOrZero
    private double carbs;

    @PositiveOrZero
    private double kilocalories;

}
