package core.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Data
@NoArgsConstructor
public class PortionDto {

    private Long id;

    @PositiveOrZero
    private double size;

    @NotNull
    private NutrientDto nutrient;

}
