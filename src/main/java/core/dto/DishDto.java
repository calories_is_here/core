package core.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
public class DishDto {

    private Long id;

    @NotBlank
    @NotNull
    private String name;

    private List<PortionDto> portions;

}
