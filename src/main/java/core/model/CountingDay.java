package core.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Entity
@NoArgsConstructor
@Data
public class CountingDay {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "counting_date")
    private LocalDate countingDate;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<Dish> dishes;

}
