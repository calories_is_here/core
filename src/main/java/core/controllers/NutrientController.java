package core.controllers;

import core.dto.NutrientDto;
import core.services.NutrientService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "${v1API}/nutrients")
@AllArgsConstructor
@Slf4j
public class NutrientController {

    private final NutrientService nutrientService;

    @GetMapping
    public ResponseEntity<List<NutrientDto>> findAll() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(nutrientService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<NutrientDto> findById(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(nutrientService.findById(id));
    }

    @PostMapping
    public ResponseEntity<Void> save(@Validated @RequestBody NutrientDto nutrientsDto) {
        nutrientService.save(nutrientsDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Long id) {
        nutrientService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
