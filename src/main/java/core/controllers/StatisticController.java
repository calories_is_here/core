package core.controllers;

import core.dto.Statistic;
import core.services.StatisticService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping(path = "${v1API}/statistics")
@AllArgsConstructor
public class StatisticController {

    private final StatisticService service;

    @GetMapping("/today")
    public ResponseEntity<Statistic> getStatisticsForToday() {
        LocalDate now = LocalDate.now();
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.getStatisticsForDate(now));
    }

    @GetMapping("/{date}")
    public ResponseEntity<Statistic> getStatisticsForDate(@PathVariable String date) {
        LocalDate localDate;
        try {
            localDate = LocalDate.parse(date);
        } catch (RuntimeException ex) {
            throw new IllegalArgumentException("Path value should be date formatted");
        }
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.getStatisticsForDate(localDate));
    }

}
