package core.controllers;

import core.dto.DishDto;
import core.services.DishService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "${v1API}/dishes")
@AllArgsConstructor
public class DishController {

    private final DishService dishService;

    @GetMapping
    public ResponseEntity<List<DishDto>> findAll() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(dishService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<DishDto> findById(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(dishService.findById(id));
    }

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody DishDto dishDto) {
        dishService.save(dishDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Long id) {
        dishService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
