package core.controllers;

import core.dto.CountingDayDto;
import core.services.CountingDayService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "${v1API}/days")
@AllArgsConstructor
public class CountingDayController {

    private final CountingDayService cds;

    @PostMapping
    public ResponseEntity<Void> save(@RequestBody CountingDayDto dayDto) {
        cds.save(dayDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Void> addDish(@RequestBody CountingDayDto dayDto) {
        cds.addDish(dayDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<CountingDayDto>> findAll() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(cds.findAll());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Long id) {
        cds.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
