package core.repositories;

import core.model.Nutrient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NutrientRepository extends CrudRepository<Nutrient, Long> {

    List<Nutrient> findAll();

}
