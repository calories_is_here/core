package core.repositories;

import core.model.CountingDay;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface CountingDayRepository extends CrudRepository<CountingDay, Long> {

    List<CountingDay> findAll();

    Optional<CountingDay> findByCountingDate(LocalDate date);

    boolean existsByCountingDate(LocalDate date);

}
