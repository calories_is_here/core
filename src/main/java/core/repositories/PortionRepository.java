package core.repositories;

import core.model.Portion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PortionRepository extends CrudRepository<Portion, Long> {

}
