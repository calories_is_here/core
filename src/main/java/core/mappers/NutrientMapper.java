package core.mappers;

import core.dto.NutrientDto;
import core.model.Nutrient;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NutrientMapper {

//    @Mapping(target = "id", ignore = true)
    Nutrient map(NutrientDto nutrientDto);

    NutrientDto mapToDto(Nutrient nutrient);

}
