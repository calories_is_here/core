package core.mappers;

import core.dto.CountingDayDto;
import core.dto.DishDto;
import core.model.CountingDay;
import core.model.Dish;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class CountingDayMapper {

    @Autowired
    private DishMapper mapper;

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "dishes", expression = "java(getDishes(dayDto))")
    public abstract CountingDay map(CountingDayDto dayDto);

    @Mapping(target = "dishes", expression = "java(getDishesDto(day))")
    public abstract CountingDayDto mapToDto(CountingDay day);

    List<Dish> getDishes(CountingDayDto dayDto) {
        if (dayDto.getDishes() == null) {
            return Collections.emptyList();
        }
        return dayDto.getDishes().stream()
                .map(mapper::map)
                .collect(Collectors.toList());
    }

    List<DishDto> getDishesDto(CountingDay day) {
        if (day.getDishes() == null) {
            return Collections.emptyList();
        }
        return day.getDishes().stream()
                .map(mapper::mapToDto)
                .collect(Collectors.toList());
    }

}
