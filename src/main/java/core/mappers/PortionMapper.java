package core.mappers;

import core.dto.NutrientDto;
import core.dto.PortionDto;
import core.model.Nutrient;
import core.model.Portion;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class PortionMapper {

    @Autowired
    private NutrientMapper mapper;

    @Mapping(target = "nutrient", expression = "java(getNutrient(portionDto))")
    abstract Portion map(PortionDto portionDto);

    @Mapping(target = "nutrient", expression = "java(getNutrientDto(portion))")
    abstract PortionDto mapToDto(Portion portion);

    Nutrient getNutrient(PortionDto portionDto) {
        return mapper.map(portionDto.getNutrient());
    }

    NutrientDto getNutrientDto(Portion portion) {
        return mapper.mapToDto(portion.getNutrient());
    }

}
