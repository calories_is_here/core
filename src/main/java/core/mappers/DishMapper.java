package core.mappers;

import core.dto.DishDto;
import core.dto.PortionDto;
import core.model.Dish;
import core.model.Portion;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class DishMapper {

    @Autowired
    private PortionMapper mapper;

    @Mapping(target = "portions", expression = "java(getPortions(dishDto))")
    public abstract Dish map(DishDto dishDto);

    @Mapping(target = "portions", expression = "java(getPortionDtos(dish))")
    public abstract DishDto mapToDto(Dish dish);

    List<Portion> getPortions(DishDto dishDto) {
        if (dishDto.getPortions() == null) {
            return Collections.emptyList();
        }
        return dishDto.getPortions().stream()
                .map(mapper::map)
                .collect(Collectors.toList());
    }

    List<PortionDto> getPortionDtos(Dish dish) {
        if (dish.getPortions() == null) {
            return Collections.emptyList();
        }
        return dish.getPortions().stream()
                .map(mapper::mapToDto)
                .collect(Collectors.toList());
    }

}
