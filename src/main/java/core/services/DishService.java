package core.services;

import core.dto.DishDto;
import core.exceptions.ResourceNotFoundException;
import core.mappers.DishMapper;
import core.model.Dish;
import core.model.Portion;
import core.repositories.DishRepository;
import core.repositories.NutrientRepository;
import core.repositories.PortionRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DishService {

    private final DishRepository dishRepo;
    private final PortionRepository portionRepo;
    private final NutrientRepository nutrientRepo;

    private final DishMapper mapper;

    @Transactional(readOnly = true)
    public List<DishDto> findAll() {
        return dishRepo.findAll().stream()
                .map(mapper::mapToDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public DishDto findById(Long id) {
        Dish dish = validate(id);
        return mapper.mapToDto(dish);
    }

    @Transactional
    public void save(DishDto dishDto) {
        Dish dish = mapper.map(dishDto);
        List<Portion> existing = new ArrayList<>();
        for (Portion portion : dish.getPortions()) {
            Long id = portion.getNutrient().getId();
            if (id != null && nutrientRepo.existsById(id)) {
                existing.add(portion);
            }
        }
        Iterable<Portion> portions = portionRepo.saveAll(existing);
        dish.setPortions(toList(portions));
        dishRepo.save(dish);
    }

    @Transactional
    public void deleteById(Long id) {
        validate(id);
        dishRepo.deleteById(id);
    }

    private Dish validate(Long id) {
        if (id == null) {
            throw new ResourceNotFoundException("attempt to find dish with id [null]");
        }
        return dishRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("dish with id [" + id + "] is not founded"));
    }

    private List<Portion> toList(Iterable<Portion> portions) {
        List<Portion> list = new ArrayList<>();
        for (Portion portion : portions) {
            list.add(portion);
        }
        return list;
    }

}
