package core.services;

import core.dto.CountingDayDto;
import core.exceptions.ResourceAlreadyExists;
import core.exceptions.ResourceNotFoundException;
import core.mappers.CountingDayMapper;
import core.model.CountingDay;
import core.model.Dish;
import core.repositories.CountingDayRepository;
import core.repositories.DishRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CountingDayService {

    private final CountingDayRepository repo;
    private final DishRepository dishRepo;
    private final CountingDayMapper mapper;

    @Transactional
    public void save(CountingDayDto dayDto) {
        if (repo.existsByCountingDate(dayDto.getCountingDate())) {
            throw new ResourceAlreadyExists("counting day with date [" + dayDto.getCountingDate().toString() + "] already exists");
        }
        CountingDay day = mapper.map(dayDto);
        repo.save(day);
    }

    @Transactional
    public void addDish(CountingDayDto dayDto) {
        List<Dish> existing = mapper.map(dayDto).getDishes().stream()
                .filter(dish -> dishRepo.existsById(dish.getId()))
                .collect(Collectors.toList());

        CountingDay day = repo.findById(dayDto.getId())
                .orElseThrow(() -> new ResourceNotFoundException("counting day with id [" + dayDto.getId() + "] is not founded"));

        day.getDishes().addAll(existing);
        repo.save(day);
    }

    @Transactional(readOnly = true)
    public List<CountingDayDto> findAll() {
        return repo.findAll().stream()
                .map(mapper::mapToDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public CountingDayDto findById(Long id) {
        CountingDay day = repo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("counting day with id [" + id + "] is not founded"));
        return mapper.mapToDto(day);
    }

    @Transactional
    public void deleteById(Long id) {
        repo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("counting day with id [" + id + "] is not founded"));
        repo.deleteById(id);
    }

}
