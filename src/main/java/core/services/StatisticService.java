package core.services;

import core.dto.Ratio;
import core.dto.Statistic;
import core.exceptions.ResourceNotFoundException;
import core.model.CountingDay;
import core.model.Dish;
import core.model.Portion;
import core.repositories.CountingDayRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@AllArgsConstructor
public class StatisticService {

    private final CountingDayRepository repo;

    public Statistic getStatisticsForDate(LocalDate date) {
        CountingDay day = repo.findByCountingDate(date)
                .orElseThrow(() -> new ResourceNotFoundException("no statistics available for the specified date [" + date + "]"));

        return aggregateStatistic(day);
    }

    private Statistic aggregateStatistic(CountingDay day) {
        double proteins = 0.0;
        double fats = 0.0;
        double carbs = 0.0;
        double kilocalories = 0.0;

        for (Dish dish : day.getDishes()) {
            for (Portion portion : dish.getPortions()) {
                double size = portion.getSize();
                double curProteins = portion.getNutrient().getProteins();
                double curFats = portion.getNutrient().getFats();
                double curCarbs = portion.getNutrient().getCarbs();
                double curKC = portion.getNutrient().getKilocalories();

                proteins += curProteins * size;
                fats += curFats * size;
                carbs += curCarbs * size;
                kilocalories += curKC * size;
            }
        }
        return Statistic.builder()
                .date(day.getCountingDate())
                .proteins(proteins)
                .fats(fats)
                .carbs(carbs)
                .kilocalories(kilocalories)
                .ratio(new Ratio(proteins, fats, carbs))
                .build();
    }

}
