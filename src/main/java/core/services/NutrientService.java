package core.services;

import core.dto.NutrientDto;
import core.exceptions.ResourceNotFoundException;
import core.mappers.NutrientMapper;
import core.model.Nutrient;
import core.repositories.NutrientRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class NutrientService {

    private final NutrientRepository repository;
    private final NutrientMapper mapper;

    @Transactional(readOnly = true)
    public List<NutrientDto> findAll() {
        return repository.findAll().stream()
                .map(mapper::mapToDto)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public NutrientDto findById(Long id) {
        Nutrient nutrient = repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("nutrient with id [" + id + "] is not founded"));
        return mapper.mapToDto(nutrient);
    }

    @Transactional
    public void save(NutrientDto nutrientDto) {
        Nutrient nutrient = mapper.map(nutrientDto);
        repository.save(nutrient);
    }

    @Transactional
    public void saveAll(List<NutrientDto> nutrientsDto) {
        List<Nutrient> nutrients = nutrientsDto.stream()
                .map(mapper::map)
                .collect(Collectors.toList());
        repository.saveAll(nutrients);
    }

    @Transactional
    public void deleteById(Long id) {
        repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("nutrient with id [" + id + "] is not founded"));
        repository.deleteById(id);
    }

}
