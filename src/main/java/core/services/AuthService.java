package core.services;

import core.dto.AuthenticationResponse;
import core.dto.LoginRequest;
import core.dto.RefreshTokenRequest;
import core.dto.RegisterRequest;
import core.model.Authority;
import core.model.User;
import core.repositories.AuthoritiesRepository;
import core.repositories.UserRepository;
import core.security.JwtProvider;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Collections;

@Service
@AllArgsConstructor
public class AuthService {

    private final UserRepository userRepo;
    private final AuthoritiesRepository authoritiesRepo;
    private final AuthenticationManager authManager;
    private final PasswordEncoder passwordEncoder;
    private final JwtProvider jwtProvider;
    private final RefreshTokenService refreshTokenService;

    @Transactional
    public void register(RegisterRequest request) {
        User user = new User();
        user.setUsername(request.getUsername());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setEmail(request.getEmail());
        user.setEnabled(true);
        user.setAuthorities(Collections.emptyList());
        user.setCreated(Instant.now());
        User saved = userRepo.save(user);

        Authority authority = new Authority();
        authority.setUser(saved);
        authority.setName(User.USER);
        authoritiesRepo.save(authority);
    }

    public AuthenticationResponse login(LoginRequest request) {
        Authentication userPassword = new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword());
        Authentication authentication = authManager.authenticate(userPassword);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtProvider.generateToken(authentication);
        return AuthenticationResponse.builder()
                .authenticationToken(token)
                .refreshToken(refreshTokenService.generateRefreshToken().getToken())
                .expiresAt(Instant.now().plusMillis(jwtProvider.getJwtExpirationInMillis()))
                .username(request.getUsername())
                .build();
    }

    @Transactional(readOnly = true)
    public User getCurrentUser() {
        var principal = SecurityContextHolder.
                getContext().getAuthentication().getPrincipal();

        if (principal instanceof org.springframework.security.core.userdetails.User) {
            var asUser = (org.springframework.security.core.userdetails.User) principal;
            return userRepo.findByUsername(asUser.getUsername())
                    .orElseThrow(() -> new UsernameNotFoundException("User name not found - " + asUser.getUsername()));
        } else {
            throw new UsernameNotFoundException("Can not identify user");
        }
    }

    public AuthenticationResponse refreshToken(RefreshTokenRequest refreshTokenRequest) {
        refreshTokenService.validateRefreshToken(refreshTokenRequest.getRefreshToken());
        String token = jwtProvider.generateTokenWithUsername(refreshTokenRequest.getUsername());
        return AuthenticationResponse.builder()
                .authenticationToken(token)
                .refreshToken(refreshTokenRequest.getRefreshToken())
                .expiresAt(Instant.now().plusMillis(jwtProvider.getJwtExpirationInMillis()))
                .username(refreshTokenRequest.getUsername())
                .build();
    }

}
